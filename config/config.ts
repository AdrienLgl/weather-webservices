import dotenv from 'dotenv';

export interface Server {
    hostname: string | null;
    user: string | null;
    password: string | null;
    port: string | number;
    dbName: string;
}

dotenv.config();

const HOST: string = process.env.DB_HOST || 'localhost';
const DB_NAME: string = process.env.DB_NAME || 'weather';
const DB_USER: string | null = process.env.DB_USER || null;
const DB_PASS: string | null = process.env.DB_PASS || '';
const DB_PORT: string | number = process.env.DB_PORT || 3306;

const SERVER: Server = {
    hostname: HOST,
    user: DB_USER,
    password: DB_PASS,
    port: DB_PORT,
    dbName: DB_NAME
}

export default SERVER;