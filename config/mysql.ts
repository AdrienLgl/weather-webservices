import mysql from 'mysql';
import config from './config';

const params = {
    user: config.user?.toString(),
    password: config.password?.toString(),
    host: config.hostname?.toString(),
    database: config.dbName
};


const connection = mysql.createConnection(params);
// open the MySQL connection
connection.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});


export { connection };
