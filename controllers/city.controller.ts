import { Request, Response } from 'express';
import { Weather } from '../_helpers/weather.model';
import CityModel from '../models/city.model';

exports.findOne = (req: Request, res: Response) => {
    const city = new CityModel(req.query.zipCode?.toString() || '');
    city.findByZipCode((err: any, data: any) => {
        const result = {
            status: 200,
            city: data
        };
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found City with id.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving City with id "
                });
            }
        } else res.send(result);
    });
};


exports.createOne = (res: Response, weather: Weather) => {
    const city = new CityModel(weather.zip_code || '');
    city.createOneCity(weather, (err: Error, data: any) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        else res.status(200);
    });
};