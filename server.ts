import express from "express";
require('dotenv').config('./.env');
var route = require('./routes/city.route');

export interface ProcessEnv {
    HOST: string;
    PORT: string;
    API_KEY: string;
}

const app = express();
const PORT: string = process.env.PORT || '4000';

app.use(route);
app.use((req, res, next) => {
    const error = new Error('Not found');
    res.status(404).json({
        message: error.message
    });
})

app.listen(PORT, () => console.log(`Server is running here : https://localhost:${PORT}`));