import { Weather } from '../_helpers/weather.model';
import Http from 'http';
import { resolve } from 'path';

const KELVIN = 273.15;


class WeatherService {

    API_KEY: string;
    local: string;
    finalWeather: Weather = {
        actual_temperature: 0,
        max_temperature: 0,
        min_temperature: 0,
        weather: '',
        zip_code: ''
    }

    constructor() {
        this.API_KEY = process.env.API_KEY || '';
        this.local = process.env.LOCAL_API || 'fr';
    }

    public handleResult(result: any, zipCode: string): Weather {
        const weather: Weather = {
            zip_code: zipCode,
            actual_temperature: parseInt(result['main']['temp']) - KELVIN,
            max_temperature: parseInt(result['main']['temp_max']) - KELVIN,
            min_temperature: parseInt(result['main']['temp_min']) - KELVIN,
            weather: result['weather'][0]['main']
        };
        return weather;
    }


    public getWeather(zipCode: string): any {
        return new Promise(() => {
            Http.get(`http://api.openweathermap.org/data/2.5/weather?q=${zipCode},fr&appid=${this.API_KEY}`, (res) => {
                if (res.statusCode === 200) {
                    let data = '';
                    res.on('data', (chunk) => {
                        data += chunk;
                    });
                    res.on('end', () => {
                        data = JSON.parse(data);
                        this.finalWeather = this.handleResult(data, zipCode);
                        resolve(JSON.stringify(this.finalWeather));
                    });
                    res.on('error', (err) => {
                        resolve(JSON.stringify(this.finalWeather));
                        console.log('Error : ' + err);
                    });
                }
            });

        })
    }
}

export default WeatherService;