export interface Weather {
    zip_code: string;
    actual_temperature: number;
    min_temperature: number;
    max_temperature: number;
    weather: string;
}