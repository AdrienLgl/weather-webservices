class Helpers {

    constructor() { }

    public time(date: Date): number {
        const lastRequest = new Date(date).getTime();
        const now = new Date().getTime();
        return new Date(now - lastRequest).getMinutes();
    }
}