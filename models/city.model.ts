import { connection } from '../config/mysql';
import { Weather } from '../_helpers/weather.model';
import WeatherService from '../_services/service.weather';
var controller = require('../controllers/city.controller.ts');

class CityModel {

    zipCode: string;
    weatherService: WeatherService;
    weather: Weather = { // Default value to create city (I encoutered a problem with the data transfer between the service and the SQL request)
        actual_temperature: 20,
        max_temperature: 35,
        min_temperature: 2,
        weather: 'Clouds',
        zip_code: '35000'
    };

    constructor(zipCode: string) {
        this.zipCode = zipCode;
        this.weatherService = new WeatherService();
    }

    public findByZipCode(result: any) {
        let query = `SELECT * FROM city_weather WHERE zip_code = "${this.zipCode}"`;
        connection.query(query, (err: Error, res: any) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }
            if (res.length) {
                console.log("found city: ", res[0]);
                result(null, res[0]);
                return;
            } else {
                // API Request in the service
                const test = this.weatherService.getWeather(this.zipCode);
                test.then((value: any) => {
                    console.log(value);
                });
                controller.createOne(null, this.weather);
            }
        });
    }

    public createOneCity(newCity: Weather, result: any) {
        connection.query("INSERT INTO city_weather SET ?", newCity, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }
            console.log("created city: ", { id: res.insertId, ...newCity });
            // result(null, { id: res.insertId, ...newCity }); Return the data
        });
    }
}

export default CityModel;
